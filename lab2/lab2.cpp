#include <iostream>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <float.h>
#include <time.h>
#include "mpi.h"

#define TAG 37

void KohonenClustering(double** data, int numOfPoints, int dimension,
	double*** clusters, int gridHeight, int gridWidth, int** clusterID);

void SOMTraining(double** data, int numOfPoints, int dimension,
	double*** clusters, int gridHeight, int gridWidth);

double EuclidDistance(double** data, int dimension, int firstIndex, int secondIndex);
double EuclidDistance(double* firstVector, double* secondVector, int dimension);

double distFunc(int Xwinner, int Ywinner, int x, int y);

void normalizeData(double** data);

const int dimension_count = 10;
const int vectors_count = 19020;

const int grid_width = 3;
const int grid_height = 3;

const int clusters_count = grid_height * grid_width;

int main(int argc, char* argv[])
{
	srand(time(NULL));

	int i, rank, numproc;
	MPI_Status status;

	MPI_Init(&argc, &argv);

	MPI_Comm_size(MPI_COMM_WORLD, &numproc);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);


	double *data_array = new double[dimension_count * vectors_count];

	double** data = new double*[vectors_count];
	for (int i = 0; i < vectors_count; i++)
	{
		data[i] = new double[dimension_count];
	}

	if (rank == 0)
	{
		FILE* file;
		file = fopen("magic04.data", "r");
		if (file == NULL)
		{
			return -1;
		}

		/*
			Input data
		*/
		printf("File opened\n");

		for (int i = 0; i < vectors_count; i++)
		{
			char temp_char;

			fscanf(file, "%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%c\n", &data[i][0], &data[i][1], &data[i][2], &data[i][3], &data[i][4],
				&data[i][5], &data[i][6], &data[i][7], &data[i][8], &data[i][9], &temp_char);
			/*printf("%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf\n", data[i][0], data[i][1], data[i][2], data[i][3],
				data[i][4], data[i][5], data[i][6], data[i][7], data[i][8], data[i][9]);*/
		}

		fclose(file);

		/*
			Normalize data
		*/
		normalizeData(data);

		/*
			Output data
		for (int i = 0; i < vectors_count; i++)
		{
			printf("%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf\n", data[i][0], data[i][1], data[i][2], data[i][3],
				data[i][4], data[i][5], data[i][6], data[i][7], data[i][8], data[i][9]);
		}
		*/

		for (int i = 0; i < vectors_count; i++)
		{
			for (int j = 0; j < dimension_count; j++)
			{
				data_array[i*dimension_count + j] = data[i][j];
			}
		}
	}

	MPI_Bcast(data_array, dimension_count * vectors_count, MPI_DOUBLE, 0, MPI_COMM_WORLD);

	for (int i = 0; i < dimension_count * vectors_count; i++)
	{
		data[i / dimension_count][i % dimension_count] = data_array[i];
	}

	double*** best_clusters;
	double min_dann_index = DBL_MAX;

	for (int iteration = 0; iteration < 5; iteration++)
	{

		/*
			Init clusters data
		*/
		double*** clusters = new double**[grid_height];
		for (int row = 0; row < grid_height; row++)
		{
			clusters[row] = new double*[grid_width];
			for (int col = 0; col < grid_width; col++)
			{
				clusters[row][col] = new double[dimension_count];
				for (int dim = 0; dim < dimension_count; dim++)
				{
					clusters[row][col][dim] = 0;
				}
			}
		}

		/*
			Train net
		*/
		SOMTraining(data, vectors_count, dimension_count, clusters, grid_height, grid_width);

		/*
			Получаем идентификаторы кластеров, соотвествующих каждому входному вектору
		*/
		int* clusterIds;
		KohonenClustering(data, vectors_count, dimension_count, clusters, grid_height, grid_width, &clusterIds);

		int **vector_ids_in_clusters = new int*[clusters_count];
		int *count_vectors_in_clusters = new int[clusters_count];

		double max_cluster_diameter = 0.0;
		// для каждого кластера ищем диаметры
		for (int row = 0; row < grid_height; row++)
		{
			for (int col = 0; col < grid_width; col++)
			{
				int cluster_id = row * grid_width + col;

				// считаем количество векторов в кластере
				int vectors_in_cluster_count = 0;
				for (int vector_id = 0; vector_id < vectors_count; vector_id++)
				{
					if (clusterIds[vector_id] == cluster_id)
					{
						vectors_in_cluster_count++;
					}
				}
				count_vectors_in_clusters[cluster_id] = vectors_in_cluster_count;

				// запоминаем индексы векторов в кластере
				int *vectorIds_in_cluster = new int[vectors_in_cluster_count];
				vector_ids_in_clusters[cluster_id] = vectorIds_in_cluster;
				for (int vector_id = 0, vectorInClusterId = 0; vector_id < vectors_count; vector_id++)
				{
					if (clusterIds[vector_id] == cluster_id)
					{
						vectorIds_in_cluster[vectorInClusterId] = vector_id;
						vectorInClusterId++;
					}
				}

				double cluster_diameter = 0.0;
				// находим максимальное расстояние между векторами внутри кластера
				for (int vectorInClusterId = 0; vectorInClusterId < vectors_in_cluster_count; vectorInClusterId++)
				{
					int first_vector_id = vectorIds_in_cluster[vectorInClusterId];
					for (int secondVectorInClusterId = 0; secondVectorInClusterId < vectors_in_cluster_count; secondVectorInClusterId++)
					{
						int second_vector_id = vectorIds_in_cluster[secondVectorInClusterId];

						if (first_vector_id == second_vector_id)
						{
							continue;
						}

						double distance = EuclidDistance(data, dimension_count, first_vector_id, second_vector_id);

						if (distance > cluster_diameter)
						{
							cluster_diameter = distance;
						}
					}
				}

				if (cluster_diameter > max_cluster_diameter)
				{
					max_cluster_diameter = cluster_diameter;
				}
			}
		}

		// найти минимальное расстояние
		// между векторами первого кластера и векторами второго
		double **between_clusters_distance = new double*[clusters_count];
		for (int cluster_id = 0; cluster_id < clusters_count; cluster_id++)
		{
			between_clusters_distance[cluster_id] = new double[clusters_count];
		}
		for (int first_row = 0; first_row < grid_height; first_row++)
		{
			for (int first_col = 0; first_col < grid_width; first_col++)
			{
				int first_cluster_index = first_row * grid_width + first_col;

				for (int second_row = 0; second_row < grid_height; second_row++)
				{
					for (int second_col = 0; second_col < grid_width; second_col++)
					{
						int second_cluster_index = second_row * grid_width + second_col;

						double min_distance_between_clusters = DBL_MAX;
						int first_cluster_vectors_count = count_vectors_in_clusters[first_cluster_index];
						for (int first_vector_in_cluster = 0; first_vector_in_cluster < first_cluster_vectors_count; first_vector_in_cluster++)
						{
							int second_cluster_vectors_count = count_vectors_in_clusters[second_cluster_index];
							for (int second_vector_in_cluster = 0; second_vector_in_cluster < second_cluster_vectors_count; second_vector_in_cluster++)
							{
								int first_vector_id = vector_ids_in_clusters[first_cluster_index][first_vector_in_cluster];
								int second_vector_id = vector_ids_in_clusters[second_cluster_index][second_vector_in_cluster];

								double distance = EuclidDistance(data, dimension_count, first_vector_id, second_vector_id);

								if (distance < min_distance_between_clusters)
								{
									min_distance_between_clusters = distance;
								}
							}
						}

						between_clusters_distance[first_cluster_index][second_cluster_index] = min_distance_between_clusters;
						between_clusters_distance[second_cluster_index][first_cluster_index] = min_distance_between_clusters;
					}
				}
			}
		}

		// очистить память под выделенные индексы
		for (int cluster_id = 0; cluster_id < clusters_count; cluster_id++)
		{
			delete vector_ids_in_clusters[cluster_id];
		}
		delete vector_ids_in_clusters;
		delete count_vectors_in_clusters;

		for (int row = 0; row < grid_height; row++)
		{
			for (int col = 0; col < grid_width; col++)
			{
				delete clusters[row][col];
			}
			delete clusters[row];
		}
		delete clusters;

		double min_division_value = DBL_MAX;
		for (int first_cluster_id = 0; first_cluster_id < clusters_count; first_cluster_id++)
		{
			for (int second_cluster_id = 0; second_cluster_id < clusters_count; second_cluster_id++)
			{
				if (first_cluster_id == second_cluster_id)
				{
					continue;
				}

				double division_value = between_clusters_distance[first_cluster_id][second_cluster_id] / max_cluster_diameter;

				if (division_value < min_division_value)
				{
					min_division_value = division_value;
				}
			}
		}

		// очистить память для расстояний между кластерами
		for (int cluster_id = 0; cluster_id < clusters_count; cluster_id++)
		{
			delete between_clusters_distance[cluster_id];
		}
		delete between_clusters_distance;

		if (min_division_value < min_dann_index)
		{
			min_dann_index = min_division_value;
			best_clusters = clusters;
		}
	}

	printf("Dann index = %lf\n", min_dann_index);

	/*
		Выводим центры кластеризации
	for (int row = 0; row < grid_height; row++)
	{
		for (int col = 0; col < grid_width; col++)
		{
			printf("cluster %d %d = (", row, col);

			for (int dim = 0; dim < dimension_count; dim++)
			{
				printf("%lf ", clusters[row][col][dim]);
			}
			printf(")\n");
		}
	}
	*/

	double* dann_indexes = new double[numproc];
	for (int i = 0; i < numproc; i++)
	{
		dann_indexes[i] = 0.0;
	}


	MPI_Gather(
		&min_dann_index, 1, MPI_DOUBLE,
		dann_indexes, 1, MPI_DOUBLE,
		0, MPI_COMM_WORLD);

	if (rank == 0)
	{
		double min_index = DBL_MAX;

		for (int index_number = 0; index_number < numproc; index_number++)
		{
			printf("received index value %lf\n", dann_indexes[index_number]);
			if (min_index > dann_indexes[index_number])
			{
				min_index = dann_indexes[index_number];
			}
		}

		printf("Minimal dann index equals %lf", min_index);
	}

	delete dann_indexes;

	MPI_Finalize();

	// 7494

	return 0;
}

void normalizeData(double** data)
{
	double* mins = new double[dimension_count];
	double* maxs = new double[dimension_count];
	for (int dim = 0; dim < dimension_count; dim++)
	{
		mins[dim] = DBL_MAX;
		maxs[dim] = DBL_MIN;
	}

	for (int i = 0; i < vectors_count; i++)
	{
		for (int j = 0; j < dimension_count; j++)
		{
			if (data[i][j] < mins[j])
			{
				mins[j] = data[i][j];
			}
			if (data[i][j] > maxs[j])
			{
				maxs[j] = data[i][j];
			}
		}
	}

	for (int i = 0; i < vectors_count; i++)
	{
		for (int dimension = 0; dimension < dimension_count; dimension++)
		{
			data[i][dimension] = (data[i][dimension] - mins[dimension]) / (maxs[dimension] - mins[dimension]);
		}
	}
}




double distFunc(int Xwinner, int Ywinner, int x, int y)
{
	double sig = 2;
	return exp(-(pow((double)(Xwinner - x), 2) + pow((double)(Ywinner - y), 2)) / 2 * pow(sig, 2));
}

double EuclidDistance(double** data, int dimension, int firstIndex, int secondIndex)
{
	return EuclidDistance(data[firstIndex], data[secondIndex], dimension);
}

double EuclidDistance(double* firstVector, double* secondVector, int dimension)
{
	double distance = 0;
	for (int i = 0; i < dimension; i++)
		distance += (firstVector[i] - secondVector[i]) * (firstVector[i] - secondVector[i]);
	return distance;
}

void SOMTraining(double** data, int numOfPoints, int dimension,
	double*** clusters, int gridHeight, int gridWidth)
{
	double** distance = new double*[gridHeight];
	for (int i = 0; i < gridHeight; i++)
	{
		distance[i] = new double[gridWidth];
	}

	double training_speed;
	double maxSpeed = 0.7;
	double minSpeed = 0.0001;
	int numOfIterations = 10000;
	for (int iteration_ID = 0; iteration_ID < numOfIterations; iteration_ID++)
	{
		int selected_vector_ID = rand() % numOfPoints;
		int winner_IDx = 0;
		int winner_IDy = 0;
		for (int k = 0; k < dimension; k++)
			distance[winner_IDy][winner_IDx] += (data[selected_vector_ID][k] - clusters[winner_IDy][winner_IDx][k]) *
			(data[selected_vector_ID][k] - clusters[winner_IDy][winner_IDx][k]);
		for (int i = 0; i < gridHeight; i++)
		{
			for (int j = 0; j < gridWidth; j++)
			{
				for (int k = 0; k < dimension; k++)
					distance[i][j] += (data[selected_vector_ID][k] - clusters[i][j][k]) *
					(data[selected_vector_ID][k] - clusters[i][j][k]);
				if (distance[winner_IDy][winner_IDx] > distance[i][j])
				{
					winner_IDy = i;
					winner_IDx = j;
				}
			}
		}
		training_speed = maxSpeed - (maxSpeed - minSpeed) * iteration_ID / numOfIterations;

		for (int i = 0; i < gridHeight; i++)
			for (int j = 0; j < gridWidth; j++)
				for (int k = 0; k < dimension; k++)
					clusters[i][j][k] += training_speed * distFunc(winner_IDx, winner_IDy, j, i) *
					(data[selected_vector_ID][k] - clusters[i][j][k]);

		for (int i = 0; i < gridHeight; i++)
			for (int j = 0; j < gridWidth; j++)
				distance[i][j] = 0;

	}
}

void KohonenClustering(double** data, int numOfPoints, int dimension,
	double*** clusters, int gridHeight, int gridWidth, int** clusterID)
{
	int numOfClusters = gridHeight * gridWidth;

	double* distance = new double[numOfClusters];

	*clusterID = new int[numOfPoints];
	int* count = new int[numOfClusters];

	for (int p = 0; p < numOfPoints; p++)
	{
		int xIndexOfWinner = 0;
		int yIndexOfWinner = 0;

		int indexOfMinDistance = 0;
		distance[0] = 0;
		for (int k = 0; k < dimension; k++)
			distance[0] += (data[p][k] - clusters[yIndexOfWinner][xIndexOfWinner][k]) *
			(data[p][k] - clusters[yIndexOfWinner][xIndexOfWinner][k]);

		for (int i = 0; i < gridHeight; i++)
			for (int j = 0; j < gridWidth; j++)
			{
				int indexOfCluster = i * gridWidth + j;
				distance[indexOfCluster] = 0;
				for (int k = 0; k < dimension; k++)
					distance[indexOfCluster] += (data[p][k] - clusters[i][j][k]) *
					(data[p][k] - clusters[i][j][k]);
				if (distance[indexOfCluster] < distance[indexOfMinDistance])
				{
					indexOfMinDistance = indexOfCluster;
					xIndexOfWinner = j;
					yIndexOfWinner = i;
				}
			}
		(*clusterID)[p] = indexOfMinDistance;
	}
}
